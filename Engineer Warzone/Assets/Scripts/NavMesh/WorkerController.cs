using UnityEngine;
using UnityEngine.AI;

public class WorkerController : MonoBehaviour
{
    public Camera cam;

    public NavMeshAgent agent;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                print(hit.point);
                agent.SetDestination(hit.point);
            }
        }
    }
}